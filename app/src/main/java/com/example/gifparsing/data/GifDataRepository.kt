package com.example.gifparsing.data

import com.example.gifparsing.data.dataSource.GifData
import io.reactivex.Single

interface GifDataRepository {
    fun getGif(appKey: String, limit: Int): Single<List<GifData>>
}