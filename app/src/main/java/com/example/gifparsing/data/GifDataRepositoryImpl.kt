package com.example.gifparsing.data

import com.example.gifparsing.data.dataSource.GifData
import com.example.gifparsing.data.dataSource.GifDataSource
import com.example.gifparsing.data.networkCalls.APIController
import io.reactivex.Single

class GifDataRepositoryImpl(
    private val newDataSource: GifDataSource = APIController()
) : GifDataRepository {
    /**
     * ключ и количество получаемых Gif
     */
//    fun getGif(): Single<List<GifData>> =
//        newDataSource.getGif("lTPHKxEBVXJelZOuyrQHmoFZmuPr16Xc", 10)

    override fun getGif(appKey: String, limit: Int): Single<List<GifData>> =
        newDataSource.getGif(appKey, limit)
            .map { gifInfo -> gifInfo.data }
}