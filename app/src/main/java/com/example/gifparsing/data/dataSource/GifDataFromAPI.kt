package com.example.gifparsing.data.dataSource

import com.google.gson.annotations.SerializedName

data class GifFromAPI (
    @SerializedName("data") val data: List<GifData>
)

data class GifData (
    @SerializedName("id") val id: String,
    @SerializedName("url") val url: String,
    @SerializedName("images") val images: ImageList
)

data class ImageList (
    @SerializedName("fixed_height") val fixed_height: FixedHeight
)

data class FixedHeight (
    @SerializedName("url") val url: String
)