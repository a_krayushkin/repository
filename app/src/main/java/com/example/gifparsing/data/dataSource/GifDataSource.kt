package com.example.gifparsing.data.dataSource

import io.reactivex.Single

interface GifDataSource {
    fun getGif(appKey: String, limit: Int): Single<GifFromAPI>
}