package com.example.gifparsing.fragment

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.gifparsing.data.dataSource.GifData
import com.example.gifparsing.databinding.ItemGifBinding
import com.example.gifparsing.util.Constants.OPERATION_SHARE_MESSENGER

/**
 * Адатер получает позицию гифки
 */
typealias OnGifClickListener = (data: GifData, position: Int, operation: Int) -> Unit

class GifAdapter (private var listener: OnGifClickListener) : RecyclerView.Adapter<GifAdapter.GifViewHolder>() {
//    private var listURI = mutableListOf<GifFromAPI>()
    var listURI: List<GifData> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GifViewHolder =
        GifViewHolder(ItemGifBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        ))

    override fun onBindViewHolder(holder: GifViewHolder, position: Int) {
        holder.bind(listURI[position], listener)
    }

    override fun getItemCount() = listURI.size

    /**
     * ID и Gif-анимация подставляется в RecyclerView
     */
    class GifViewHolder(private val binding: ItemGifBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(gifUri: GifData, listener: OnGifClickListener) {
            Glide.with(binding.root.context)
                .asGif()
                .load(gifUri.images.fixed_height.url)
                .into(binding.itemGif)
//            binding.itemGifTextInfo.text = gifUri.data[adapterPosition].id
//            Log.v(LOG_VIEW, "${gifUri.data[adapterPosition].id}")
            binding.root.setOnClickListener {
                listener.invoke(gifUri, adapterPosition, OPERATION_SHARE_MESSENGER)
            }
        }
    }

//    fun addGif(gifList: GifFromAPI) {
//        listURI.add(gifList)
//        Log.v(LOG_VIEW, "list size = ${listURI.size}, $listURI")
//        notifyDataSetChanged()
//    }
}