package com.example.gifparsing.fragment

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.gifparsing.R
import com.example.gifparsing.data.dataSource.GifData
import com.example.gifparsing.databinding.FragmentGifsListBinding
import com.example.gifparsing.util.Constants.LOG_VIEW
import com.example.gifparsing.util.Constants.OPERATION_SHARE_MESSENGER
import com.example.gifparsing.viewModel.GifViewModel

class GifFragment : Fragment(R.layout.fragment_gifs_list) {
    private lateinit var binding: FragmentGifsListBinding
    private lateinit var viewModelProvider: ViewModelProvider
    private lateinit var loadAdapter: GifAdapter
    private lateinit var gifClickListener: OnGifClickListener

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentGifsListBinding.bind(view)
        viewModelProvider = ViewModelProvider(this)
        viewModelProvider
            .get(GifViewModel::class.java)
            .also {
//            it.gifLiveData.observe(viewLifecycleOwner, {data -> loadAdapter(data)})
            it.gifListLiveData.observe(viewLifecycleOwner, {data -> showContent(data)})
            it.fetchGifList()
        }

        /**
         * Передача выбранной гифки в виде ссылки
         */
        gifClickListener = object : OnGifClickListener{
            override fun invoke(data: GifData, position: Int, operation: Int) {
                when(operation) {
                    OPERATION_SHARE_MESSENGER -> {
                        val thisGit = data.images.fixed_height.url
                        val intent = Intent().apply {
                            action = Intent.ACTION_SEND
                            putExtra(Intent.EXTRA_TEXT, thisGit)
                            type = "text/plain"
                        }
                        startActivity(intent)
                    }
                }
            }
        }

        loadAdapter = GifAdapter(gifClickListener)
        binding.listGifs.apply {
            adapter = loadAdapter
            layoutManager = LinearLayoutManager(context)
        }
    }

    /**
     * проверка данных, полученных с сайта
     */
    private fun showContent(gif: List<GifData>) {
        loadAdapter.listURI = gif
        Log.v(LOG_VIEW, "size: ${gif.size} \n ${gif.get(1)}")
//        binding.apply {
//            for (i in 0..9) {
//                Log.v(LOG_VIEW, "This is id ==== ${gif.id} ////// This is URL ==== ${gif.data[i].url}\n")
//                Log.v(LOG_VIEW, "${gif.data.size}")
//            }
//        }
    }
}