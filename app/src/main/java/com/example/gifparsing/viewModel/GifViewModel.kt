package com.example.gifparsing.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.gifparsing.data.dataSource.GifData
import com.example.gifparsing.data.GifDataRepositoryImpl
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable

class GifViewModel: ViewModel() {
    private val gifRepository = GifDataRepositoryImpl()

//    private val mutableGifLiveData = MutableLiveData<GifFromAPI>()
//    var gifLiveData: LiveData<GifFromAPI> = mutableGifLiveData

    private val mutableGifListLiveData = MutableLiveData<List<GifData>>()
    var gifListLiveData: LiveData<List<GifData>> = mutableGifListLiveData

    private val compositeDisposable = CompositeDisposable()

//    fun fetchGif() {
//        gifRepository.getGif()
//            .observeOn(AndroidSchedulers.mainThread())
//            .subscribe { gif ->
//                mutableGifLiveData.value = gif
//            }.also {
//                compositeDisposable.add(it)
//            }
//    }

    fun fetchGifList() {
        gifRepository.getGif("lTPHKxEBVXJelZOuyrQHmoFZmuPr16Xc", 10)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { gif ->
                mutableGifListLiveData.value = gif
            }.also {
                compositeDisposable.add(it)
            }
    }
}